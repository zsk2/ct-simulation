#!/usr/bin/env python3

import os
import pathlib
from setuptools import setup, find_packages

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

# Get requirements
setupdir = os.path.dirname(__file__)
requirements = []
for line in open(os.path.join(setupdir, "requirements.txt"), encoding="ASCII"):
    if line.strip() and not line.startswith("#"):
        requirements.append(line)

setup(
    name='ct-simulation',
    version='1.5',
    description='A Simulation of a CT Scan with a Raspberry Pi',
    url='https://gitlab.com/zsk2/ct-simulation',
    packages=find_packages(exclude=['test']),
    author='Klocker Linus, Schulz Jonas, Zanko Stefan',
    author_email='szanko at protonmail dot com',
    license='GNU General Public License v3',
    install_requires=requirements,
    entry_points={"gui_scripts": ["ct-simulation = ct_simulation.gui:main"]},
    include_package_data=True
)
