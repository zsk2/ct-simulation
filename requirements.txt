gpiozero~=1.5.1
PyYAML~=5.3.1
pyxdg~=0.26
pytest~=6.2.1
opencv-python~=4.4.0.46
# PyQt5~=5.15.2
QtPy==1.3.1
numpy>=1.19.3
radontea~=0.4.7
matplotlib==3.0.2
picamera~=1.13
natsort~=6.0.0
