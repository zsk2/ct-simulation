#!/usr/bin/env python3
"""Tests the Hardware Interface"""

import random
import platform
import pytest

from ct_simulation import hardware
from ct_simulation.configloader import LogFile


def test_platform_checker():
    """Test if the Project runs on the correct Platform"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_platform_checker---------------')
    if platform.machine() != 'armv7l':
        with pytest.raises(AssertionError):
            hardware.platform_checker()
    else:
        assert hardware.platform_checker()


def test_fully_laser():
    """Tests the Laser Class"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_fully_laser---------------')
    if platform.machine() != 'armv7l':
        return
    test_laser = hardware.Laser()
    assert None is not test_laser
    tmp_laser = hardware.Laser.get_instance()
    assert test_laser is tmp_laser
    assert tmp_laser.laser_toggler() is False
    assert tmp_laser.laser_toggler() is True


def test_fully_turn_table():
    """Tests the Turn Table Class"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_fully_turn_table---------------')
    # TODO write actual test
    if platform.machine() != 'armv7l':
        return


def test_fully_camera():
    """Tests the Camera Class"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_fully_camera---------------')
    # TODO write actual test
    if platform.machine() != 'armv7l':
        return


def test_takepictures_noinput():
    """Tests the interface for the hardware
    with no input
    Excepts a ValueError if no input is given"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_takepictures_noinput---------------')
    with pytest.raises(ValueError):
        hardware.takepictures()


def test_takepictures_absorption():
    """Tests the interface for the hardware
    with a input for amount to take on absorption
    Excepts at this point in time nothing"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_takepictures_absorption---------------')
    amount = random.randint(1, 11)
    if platform.machine() == 'armv7l':
        hardware.takepictures(amount=amount, recon=True)


def test_takepictures_one_absorption():
    """Tests the interface for the hardware
    with different inputs for angular \
            one smaller \
            one bigger \
            one random between min and max
    Excepts at this point in time nothing"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_takepictures_one_absorption---------------')
    angular = random.randint(1, 360)
    if platform.machine() == 'armv7l':
        # Smaller than zero test
        with pytest.raises(ValueError):
            hardware.takepictures(angular=-1)
        # Bigger than 360 test
        with pytest.raises(ValueError):
            hardware.takepictures(angular=361)
        # Random generated
        hardware.takepictures(angular)


def test_takepictures_reconstruction():
    """Tests the interface for the hardware
    with a input for amount to take reconstruction
    Excepts at this point in time nothing"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_takepictures_reconstruction---------------')
    amount = random.randint(1, 11)
    if platform.machine() == 'armv7l':
        hardware.takepictures(amount=amount)
