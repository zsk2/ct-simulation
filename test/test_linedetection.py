"""Tests the LineDetection"""

from ct_simulation import linedetection
from ct_simulation.configloader import LogFile


def test_reconstruction():
    """ show an image with a sinogramm und the corresponding absorption
        profiles and the reconstructed image
    """
    logger = LogFile.get_logfile()
    logger.info('---------------test_reconstruction---------------')
    # open image to compare

    paths = []
    for i in range(2, 29):
        if i < 10:
            paths.append("ct_simulation/laser/guteBilder5/full_dice_few_dark_0" + str(i) + ".jpg")
        else:
            paths.append("ct_simulation/laser/guteBilder5/full_dice_few_dark_" + str(i) + ".jpg")

    calibrationcurves = []
    for i in range(0, 4):
        calibrationcurves.append("ct_simulation/Bilder/calibrationcurve-pictures/calibrationcurve_" + str(i) + ".jpg")

    line_detection = linedetection.LineDetection()
    aps = line_detection.reconstruction(paths, calibrationcurves, True)
    linedetection.call_gui()
