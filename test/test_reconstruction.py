"""Tests the LineDetection"""
import pytest

from ct_simulation import reconstruction
from ct_simulation.configloader import LogFile


def test_get_absorptionprofile():
    """Tests if reconstruction gets a absorptionprofile"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_get_absorptionprofile---------------')
    with pytest.raises(ValueError):
        reconstruction.get_absorptionprofile(0, 1, 0, 1, 0, 10)
