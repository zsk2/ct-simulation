import re
from ct_simulation import image_processing as imgp
from ct_simulation import configloader as conf
from ct_simulation import filehandler
import shutil


def test_SplitFrames_constructor():
    """Tests if the correct path is choose in the constructor"""
    logger = conf.LogFile.get_logfile()
    logger.info('---------------test_SplitFrames_constructor---------------')
    split: imgp.SplitFrames = imgp.SplitFrames(calibrationcurve=True)
    cfg = conf.load_config()
    logger.debug(split.imageseriespath)
    tmpmatch = re.search(cfg['Filehandler']['cali'], split.imageseriespath)
    # delete folder
    shutil.rmtree(filehandler.get_img_series_folder(newimgseries=False,
                                                    folder_name=cfg['Filehandler']['cali']))
    assert tmpmatch is not None

    split: imgp.SplitFrames = imgp.SplitFrames(calibrationcurve=False)
    tmpmatch = re.search(cfg['Filehandler']['imgs'], split.imageseriespath)
    # delete folder
    shutil.rmtree(filehandler.get_img_series_folder(newimgseries=False,
                                                    folder_name=cfg['Filehandler']['imgs']))
    assert tmpmatch is not None
