"""Tests the TurnTableThreading Stuff"""

import random
import platform
import pytest

from ct_simulation import turntable as turn
from ct_simulation.configloader import LogFile


def test_setangular_inputvalidation():
    """Test that inputs arepositiv and between 0 and 360 \
            and that the method don't turn the turntable if the angular is
            0 or 360 degrees'
    """
    logger = LogFile.get_logfile()
    logger.info('---------------test_setangular_inputvalidation---------------')
    if platform.machine() == 'armv7l':
        # Smaller than zero test
        with pytest.raises(ValueError):
            turn.set_angel(random.randint(-10000, 0))
        # 360 test
        turn.set_angel(angular=360)
        # 0 test
        turn.set_angel(angular=0)


def test_time_for_turning():
    """Tests the calculation time for method"""
    logger = LogFile.get_logfile()
    logger.info('---------------test_time_for_turning---------------')
    # Test time for half a Round
    assert 9 == turn.time_for_turning(angular=180)
