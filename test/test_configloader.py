#!/usr/bin/env python3
"""Tests that the config file is can be used created"""

import os
import pytest
import yaml
from ct_simulation import configloader as conf
from xdg import BaseDirectory


def test_get_logfile():
    """Gets one Logfile and writes something"""
    logger = conf.LogFile.get_logfile()
    logger.info('---------------test_get_logfile---------------')
    logger = conf.LogFile.get_logfile()
    logcontent = 'Logger File Test'

    logger.info(logcontent)
    with open(BaseDirectory.save_cache_path('ct-simulation')+'/debug.log') as dfile:
        line = ''
        for line in dfile:
            last_line = line
        last_line = line
    last_line = last_line.split()
    assert (last_line[-3:] == logcontent.split()) is True


def test_create_config():
    """Saves the default config"""
    logger = conf.LogFile.get_logfile()
    logger.info('---------------test_create_config---------------')
    og_path = os.getcwd()+"/ct_simulation/config/example.yml"
    tmp_path = os.getcwd()+"/ct_simulation/config/example2.yml"
    # Checks that it throws an FileNotFound error
    os.rename(og_path, tmp_path)
    with pytest.raises(FileNotFoundError):
        conf.create_config()
    os.rename(tmp_path, og_path)
    conf.create_config()
    with open(os.getcwd()+"/ct_simulation/config/example.yml", 'r') as file:
        lconf = yaml.safe_load(file)
    assert lconf is not None


def test_load_configfile():
    """Loads a test config file"""
    logger = conf.LogFile.get_logfile()
    logger.info('---------------test_load_configfile---------------')
    lconf = conf.load_config()
    with open(os.getcwd()+"/ct_simulation/config/example.yml", 'r') as file:
        loadedconf = yaml.safe_load(file)

    assert lconf == loadedconf
