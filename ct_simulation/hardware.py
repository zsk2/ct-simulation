#!/usr/bin/env python3
"""Interfaces with the Hardware"""

import numpy
import platform
# import random
import time
import threading
from picamera import PiCamera
from gpiozero import LED
from typing import Callable, Tuple, Any, List

from ct_simulation import configloader as conf
from ct_simulation import image_processing as img_proc
from ct_simulation import linedetection
from ct_simulation import turntable as turn
from ct_simulation import filehandler


def platform_checker() -> bool:
    """Checks if the Project runs on the Raspberry Pi"""
    if platform.machine() != 'armv7l':
        logger = conf.LogFile.get_logfile()
        logger.error('This part of the Project runs only on the Raspberry Pi')
        raise AssertionError('This part of the Project  \
                runs only on the Raspberry Pi')
    return True


class Laser:
    """Controlls the Hardware Laser"""
    __instance = None

    @staticmethod
    def get_instance(lasergpio: int = 0):
        """Gets the only instance of Laser"""
        if Laser.__instance is None:
            Laser(lasergpio)
        return Laser.__instance

    def __init__(self, lasergpio: int = 0) -> None:
        cfg = conf.load_config()
        self.logger = conf.LogFile.get_logfile()
        platform_checker()
        if lasergpio == 0:
            self.laser = LED(cfg["Hardware"]["LaserGPIOPin"])
        else:
            self.laser = LED(lasergpio)
        if Laser.__instance is not None:
            raise Exception("This class is a singleton!")
        Laser.__instance = self
        self.logger.info('Laser Created')

    def laser_toggler(self) -> bool:
        """Inverts the state of the laser"""
        self.laser.toggle()
        if self.laser.is_active:
            self.logger.info('Laser is Off')
            ret = False
        else:
            self.logger.info('Laser is On')
            ret = True
        return ret


class Camera:
    """Controlls the Hardware Camera"""
    __instance = None

    @staticmethod
    def get_instance():
        """Gets the only instance of Camera"""
        if Camera.__instance is None:
            Camera()
        return Camera.__instance

    def __init__(self, typeofcamera="Image") -> None:
        self.cfg = conf.load_config()
        self.logger = conf.LogFile.get_logfile()
        platform_checker()
        self.type = typeofcamera
        self.img_type = self.cfg['Hardware']['Camera']['Image']['format']
        self.cache = self.cfg['Hardware']['Camera']['Cache']
        self.camera: PiCamera = PiCamera(resolution=self.cfg['Hardware']['Camera']['Resolution'])
        self.initialize_Camera()
        if Camera.__instance is not None:
            raise Exception("Camera is a singleton!")
        Camera.__instance = self

    def take_picture(self, angular: int) -> str:
        """ Takes a picture and saves it in the file system
             Returns the path of the image after taking it
        """
        name = "example"
        if angular >= 0:
            name = str(angular)+"-angular"
        imgpath = filehandler.save_img_path(name, self.img_type)
        self.logger.info('Imgpath is %s', imgpath)
        self.camera.capture(imgpath)
        return imgpath

    def initialize_Camera(self) -> None:
        """Initialize the Camera Buffer """
        start_time = round(time.time())
        while True:
            # Camera Sensor needs about two seconds of initiation time
            if round(time.time()) - start_time >= 2:
                self.logger.info('Camera for Series Initalized')
                break

    def take_series_files(self, amount: int = 120, curve: bool = False) -> bool:
        """Take a series of images and save them"""
        # To use the Rapid Fire Mode
        # skipcq: PYL-W0612
        output = img_proc.SplitFrames(calibrationcurve=curve)
        # Gets the Time for one Round
        # skipcq: PYL-W0612
        oneround = turn.time_for_turning(angular=360)

        self.camera.start_recording(output, format='mjpeg')
        self.camera.wait_recording(oneround)
        self.camera.stop_recording()
        if curve:
            folder = self.cfg['Filehandler']['cali']
        else:
            folder = self.cfg['Filehandler']['imgs']
        self.logger.debug(f'folder is {folder}')
        filehandler.remove_unwanted_imgs(amount, folder)
        return True


def __takereference_calibration(amount: int) -> List[str]:
    cam = Camera.get_instance()
    las = Laser.get_instance()
    las.laser_toggler()
    cfg = conf.load_config()

    amount = 10

    camerathread = threading.Thread(
        target=cam.take_series_files, args=(amount, True))
    hw_thread = threading.Thread(target=turn.set_angel, args=(360,))
    hw_thread.start()
    camerathread.start()

    # Wait that the threads end
    camerathread.join()
    hw_thread.join()
    las.laser_toggler()
    filehandler.remove_unwanted_imgs(amount, foldername=cfg['Filehandler']['cali'])
    return filehandler.get_imgs(cfg['Filehandler']['cali'])


def __take_one_absorptionprofile(angular: int) -> Tuple[Any, numpy.ndarray]:
    """ Don't use this function directly go through the takepictures interface
        gets an angular and turns the TurnTable to this angular
    """
    # Turns the turntable to the specified angular
    turn.set_angel(angular)
    cam = Camera.get_instance()
    las = Laser.get_instance()
    las.laser_toggler()
    cfg = conf.load_config()

    imgpath: list = []
    imgpath.append(cam.take_picture(angular))

    start_time = round(time.time())
    while True:
        if round(time.time()) - start_time >= 2:
            break

    # Turns the turntable back to the default angular
    turn.set_angel(360-angular)
    las.laser_toggler()
    line_detection = linedetection.LineDetection()
    calibrationimgs = filehandler.get_imgs(cfg['Filehandler']['cali'])

    return line_detection.reconstruction(imgpath, calibrationimgs)


def __takemany_absorptionprofile(amount: int) -> None:
    """ Don't use this function directly go through the takepictures interface
    Takes an  amount of Absorption Images \
            and sent than the images to <++>"""
    cam = Camera.get_instance()
    las = Laser.get_instance()
    las.laser_toggler()
    cfg = conf.load_config()
    # Because the camera should take the pictures
    # while the turn table is turning
    # we should use multi threading
    camerathread = threading.Thread(
        target=cam.take_series_files, args=(amount,))
    hw_thread = threading.Thread(target=turn.set_angel, args=(360+10,))
    hw_thread.start()
    camerathread.start()

    # Wait that the threads end
    camerathread.join()
    hw_thread.join()

    las.laser_toggler()

    imgpaths = filehandler.get_imgs()
    calibrationimgs = filehandler.get_imgs(cfg['Filehandler']['cali'])

    line_detection = linedetection.LineDetection()
    return line_detection.reconstruction(imgpaths, calibrationimgs)


def __takerekonstruction(amount: int) -> None:
    """ Don't use this function directly go through the takepictures interface
    Takes an amount of reconstruction \
            and sent than the images to LineDetection"""
    cam = Camera.get_instance()
    las = Laser.get_instance()
    las.laser_toggler()
    cfg = conf.load_config()
    # Because the camera should take the pictures
    # while the turn table is turning
    # we should use multi threading
    camerathread = threading.Thread(
        target=cam.take_series_files, args=(amount,))
    hw_thread = threading.Thread(target=turn.set_angel, args=(360+10,))
    hw_thread.start()
    camerathread.start()

    # Wait that the threads end
    camerathread.join()
    hw_thread.join()
    las.laser_toggler()

    imgpaths = filehandler.get_imgs()
    calibrationimgs = filehandler.get_imgs(cfg['Filehandler']['cali'])
    line_detection = linedetection.LineDetection()
    return line_detection.reconstruction(list_pictures=imgpaths, list_calibration=calibrationimgs, do_reconstruction=True)


def takepictures(angular: int = -1, amount: int = -1, recon: bool = False, calibration: bool = False)\
        -> list:
    """Interface to talk to the Hardware
    To take one picture just give an angular the object \
            will be turned to that angular
    To take an Absorption profile give an amount and absorb = True
    To take a Reconstruction give an amount
    """

    conf.StandardCalibrationCurves.copy_pictures()
    tmplogger = conf.LogFile.get_logfile()
    if int(angular) > -1 and int(angular) < 361:
        tmplogger.debug('One Absorption Profile')
        tmp = __take_one_absorptionprofile(angular)
    elif recon is False and amount > 0 and calibration is False:
        tmplogger.debug('Absorption Profile')
        tmp = __takemany_absorptionprofile(amount)
    elif amount > 0 and recon is True and calibration is False:
        tmplogger.debug('Reconstruction')
        tmp = __takerekonstruction(amount)
    elif amount > 0 and calibration is True:
        tmplogger.debug('Calibration')
        tmp = __takereference_calibration(amount)
    else:
        raise ValueError('Amount should be greater than 0')
    return tmp


# if __name__ == "__main__":
#     __takereference_calibration()
#     las = Laser.get_instance()
#     las.laser_toggler()
#     time.sleep(3)
#     las.laser_toggler()
#     time.sleep(3)
#     angular = random.randint(1, 360)
#     print(angular)
#     takepictures(angular=angular)
#     amount = random.randint(1, 11)
#     takepictures(amount=amount)
