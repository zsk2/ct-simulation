#!/usr/bin/env python3
"""Controlls the TurnTable"""

import sys
import time
import threading
from gpiozero import LED
from ct_simulation import configloader as conf
from ct_simulation import hardware


class TurnTableThread(threading.Thread):
    """The Thread with is used to Controll the TurnTable"""

    def __init__(self, *args, **keywords):
        threading.Thread.__init__(self, *args, **keywords)
        self.killed = False

    def start(self):
        """Starts the thread"""
        self.__run_backup = self.run
        self.run = self.__run
        threading.Thread.start(self)

    def __run(self):
        sys.settrace(self.globaltrace)
        self.__run_backup()
        self.run = self.__run_backup

    def globaltrace(self, frame, event, arg):
        """I don't know what this method does"""
        if event == 'call':
            return self.localtrace
        return None

    def localtrace(self, frame, event, arg):
        if self.killed and event == 'line':
            raise SystemExit()
        return self.localtrace

    def kill(self):
        """Kills the Thread"""
        self.killed = True


def turnon(pin: int = 14) -> None:
    """The Method with turns the TurnTable on"""
    # cfg = conf.load_config()
    # pin = cfg['Hardware']['Turntable']['GPIO']
    # pin = 14
    led = LED(pin)
    led.on()
    while True:
        # Do something useless
        pin += 1


def time_for_turning(logger=None, rpm: int = 4, angular: int = 360) -> float:
    """Calculates the time for turning to a particular angular"""
    # How much does it take to finish one round
    oneround = 60 / rpm
    # How much does it take to get to the angular
    turntime = angular / 360 * oneround
    if logger is not None:
        logger.info(f'One Round is {oneround} seconds long')
        logger.info(f'The time for turning is {turntime} seconds long')
    # Multiplied bei 1.2 because it needs more time
    return turntime * 1.2


def set_angel(angular: int = 360, singleimg: bool = False) -> bool:
    """Sets the angle of the object witch lays on the turntable have to be called 2 times one for turning to the position and one for turning back to inital position"""
    # Don't turn if the position is at 0 or 360
    if angular < 0:
        raise ValueError('Amount should be greater than 0')
    if angular not in (361, 0) and singleimg is False:
        logger = conf.LogFile.get_logfile()
        cfg = conf.load_config()
        rpm = cfg['Hardware']['Turntable']['RPM']
        timeturning = time_for_turning(logger, rpm, angular)
        start_time = round(time.time(), 2)
        hardware.platform_checker()

        pin = cfg['Hardware']['Turntable']['GPIO']
        logger.info('TurnTable Created')
        # Starts the Thread however also starts at this point the thread
        hw_thread = TurnTableThread(target=turnon, args=(pin,))
        hw_thread.start()
        while True:
            if round(time.time(), 2) - start_time >= timeturning:
                logger.info(f'Angular Set to {angular}')
                hw_thread.kill()
                hw_thread.join()
                break
    return True
