"""Shows the Camera Preview with the Laser enabled"""

from gpiozero import LED  # Import Libs
import picamera

from ct_simulation import configloader as conf

if __name__ == "__main__":
    cfg = conf.load_config()
    led = LED(cfg["Hardware"]["LaserGPIOPin"])  # Uses Broadcoms Pin Labeling can be seen with pinout
    led.on()
    camera = picamera.PiCamera()
    camera.resolution = (1024, 768)
    camera.start_preview()
    tmp = 1

    # Dummy Loop to keep Stuff running
    while True:
        tmp += 1
