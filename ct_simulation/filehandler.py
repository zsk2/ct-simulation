"""Handles the Saving and Loading of Image Files"""

import cv2
import os
import math
import natsort
import random
from pathlib import Path
from typing import List
from xdg import BaseDirectory
from ct_simulation import configloader as conf


def save_img_path(name: str, img_type: str = "png", newfolder: bool = True, folder_name: str = '/img-series-') -> str:
    """ Returns the total save path for one image in a new $XDG_CACHE folder"""
    logger = conf.LogFile.get_logfile()

    if newfolder:
        path = get_img_series_folder(folder_name=folder_name) + '/' + name + '.' + img_type
    else:
        path = get_img_series_folder(folder_name=folder_name, newimgseries=False) + '/' + name + '.' + img_type

    logger.info(f'Single Picture Path is {path}')
    return path


def get_imgs(foldername: str = '/img-series-') -> List[str]:
    """ Get all imgs from the latest img Series as paths"""
    logger = conf.LogFile.get_logfile()
    folder = get_img_series_folder(newimgseries=False, folder_name=foldername)
    files: List[str] = []
    with os.scandir(folder) as dirs:
        for entry in dirs:
            files.append(folder+'/'+entry.name)
    logger.info(f'Loaded this files {files}')
    files = natsort.natsorted(files)
    return files


def get_img_series_folder(newimgseries: bool = True, folder_name: str = '/img-series-') -> str:
    """ Gets the folder for a new or current imageseries as a string
    :param newimgseries: should a new imageseries folder be created or not
    if not get the folder with the highest number
    """
    # Tries to create a folder add $XDG_CACHE/ct-simulation
    cachepath = BaseDirectory.save_cache_path('ct-simulation')

    # Default image series is at $XDG_CACHE/ct-simulation/img-series-0
    imageseriespath = cachepath + folder_name + '0'
    imageseries = 0  # Default folder number for the image series
    newest = imageseriespath
    while True:
        tmppath = str(cachepath + folder_name + str(imageseries))
        # Checks if the folder exists
        if os.path.isdir(tmppath):
            newest = tmppath
            imageseries += 1
        else:
            if False is newimgseries:
                imageseriespath = newest
                if False is os.path.isdir(newest):
                    os.makedirs(newest)
                break
            # TODO through Exception if directory could not be created
            os.makedirs(tmppath)
            imageseriespath = tmppath
            break

    logger = conf.LogFile.get_logfile()
    logger.info(f'{folder_name} Path is {imageseriespath}')
    return imageseriespath


# TODO: load pictures
def create_calibrationcurves() -> str:
    """
    Copies the default calibration curves to \
    XDG_CACHE/ct-simulation/calibrationcurve-pictures if they do not exist
    """
    logger = conf.LogFile.get_logfile()

    if os.path.isdir(BaseDirectory.xdg_cache_home + 'ct-simulation/calibrationcurve-pictures-0') is False:
        calibrationcurve_folder = BaseDirectory.save_cache_path("ct-simulation/calibrationcurve-pictures-0")

        try:
            calibrationcurves = []
            calibrationcurves_names = []
            # for i in range(0, 4):
            #     calibrationcurves.append(
            #         "ct_simulation/Bilder/calibrationcurve-pictures/calibrationcurve_" + str(i) + ".jpg")
            #     calibrationcurves_names.append("calibrationcurve_" + str(i) + ".jpg")
            for i in range(0, 9):
                calibrationcurves.append(
                    str(Path(conf.__file__).parent)+"/Bilder/calibrationcurve-pictures/calibrationcurve_" + str(i) + ".jpg")
                calibrationcurves_names.append("calibrationcurve_" + str(i) + ".jpg")
            logger.info('Read calibration curve paths')

            for i in range(0, 4):
                imgpath = calibrationcurve_folder + "/" + calibrationcurves_names[i]
                img_temp = cv2.imread(calibrationcurves[i])
                # saving
                try:
                    cv2.imwrite(imgpath, img_temp)
                except RuntimeError as run_time_error:
                    logger.error('Error in saving file')
                    raise RuntimeError(run_time_error) from run_time_error

        except FileNotFoundError as exept:
            logger.error('Example calibration curves not found')
            raise FileNotFoundError(exept) from exept
    else:
        logger.error("this should only be called once")
        raise Exception("This should only be called once")
    logger.info(f'Example calibration curves written to {calibrationcurve_folder}')
    return calibrationcurve_folder


def remove_unwanted_imgs(amount: int = 10, foldername: str = '/img-series-') -> None:
    """
    Removed unwanted  pictures from folder
    :param amount: int pictures that should remain in the folder
    """
    logger = conf.LogFile.get_logfile()
    images: List[str] = get_imgs(foldername=foldername)

    if len(images) < amount:
        logger.error(
            f'Too little images in the {foldername}, there should be {amount} but in reality there are {len(images)}')
        raise ValueError(
            f'Too little images in the {foldername}, there should be {amount} but in reality there are {len(images)}')
    if amount > 0:
        # Add the numbers which should be keep to an array
        logger.debug(f'There are {len(images)} in the folder')
        keepimgs: List[int] = []

        logger.debug(f'Awaited runs {len(images)/amount}')
        for element in range(amount):
            # logger.debug(f'{element} Run')
            tmpposition = math.floor(len(images)/amount) * element
            if tmpposition < len(images):
                # logger.debug(f'Keep {tmpposition} Image')
                keepimgs.append(tmpposition)

        logger.info(f'There will be {len(keepimgs)} imgs saved')
        # TODO remove quick and dirty fix and improve the upper lines
        # If there are not enough elements in the list add some with random to the list
        if len(keepimgs) < amount:
            logger.info('Add bonus images because there are less pictures than requirert in the array')
            # keepimgs.sort()
            # downer, upper = keepimgs[:len(keepimgs)/2], keepimgs[len(keepimgs)/2:]
            needed: int = amount - len(keepimgs)
            tmprun: int = 0
            while True:
                if tmprun is needed:
                    break
                tmp = random.randint(0, len(images))
                if tmp not in keepimgs:
                    keepimgs.append(tmp)
                    tmprun += 1
            logger.info(f'Added {tmprun} bonus imgs')

        keepimgs.sort()
        logger.debug(f'Save images at this position {keepimgs}')
        for img in range(len(images)):
            if img not in keepimgs:
                if os.path.exists(images[img]):
                    os.remove(images[img])
                else:
                    logger.error(f'{images[img]} File which would be deleted is not found')
                    raise ValueError(f'{images[img]} File which would be deleted is not found')


# if __name__ == "__main__":
#     tmp = get_imgs()
#     print(tmp)
