"""Handles the Configfiles and Logfiles"""

import os
import logging
import yaml
from pathlib import Path
from xdg import BaseDirectory
from ct_simulation import filehandler


class StandardCalibrationCurves:
    __instance = None

    @staticmethod
    def copy_pictures():
        """Get the only instance of the logfile"""
        if StandardCalibrationCurves.__instance is None:
            StandardCalibrationCurves()
        return StandardCalibrationCurves.__instance

    def __init__(self) -> None:
        """ Virtually private constructor. """
        filehandler.create_calibrationcurves()


class LogFile:
    __instance = None

    @staticmethod
    def get_logfile():
        """Get the only instance of the logfile"""
        if LogFile.__instance is None:
            LogFile()
        return LogFile.__instance

    def __init__(self) -> None:
        """ Virtually private constructor. """
        if LogFile.__instance is not None:
            raise Exception("This class is a singleton!")
        logger = logging.getLogger('Debug_Log')
        logger.setLevel(logging.DEBUG)
        formatstring = '%(asctime)s - %(filename)s - %(funcName)s - %(levelname)s - %(message)s'
        formatter = logging.Formatter(formatstring)
        loggerpath = BaseDirectory.save_cache_path('ct-simulation')
        fileh = logging.FileHandler(loggerpath+'/debug.log')
        fileh.setFormatter(formatter)
        fileh.setLevel(logging.DEBUG)
        logger.addHandler(fileh)
        LogFile.__instance = logger


def create_config() -> None:
    """Copies the default config file to \
            XDG_CONFIG/ct-simulation/rc if not exists"""
    logger = LogFile.get_logfile()
    try:
        with open(str(Path(filehandler.__file__).parent)+"/config/example.yml", 'r') as file:
            conf = yaml.safe_load(file)
            logger.info('Loaded Example Config File')
            logger.debug(conf)
    except FileNotFoundError as exept:
        logger.error('Example Config not Found')
        raise FileNotFoundError(exept) from exept
    configpath = BaseDirectory.save_config_path("ct-simulation")+"/rc"
    with open(configpath, 'w') as file:
        yaml.dump(conf, file)
        logger.info('Example Config written to ~/.config/ct-simulation/rc')


def load_config():
    """Tries to load the config and if not exist create one config tries to
    create a file 5 times but then stops it"""
    logger = LogFile.get_logfile()
    tries = 0
    while True:
        try:
            configpath = BaseDirectory.save_config_path("ct-simulation")+"/rc"
            with open(configpath) as file:
                conf = yaml.safe_load(file)
                logger.info('Config File Loaded')
                break
        except FileNotFoundError:
            conf = None
            logger.error("File not accessible")
            create_config()
            if tries == 5:
                logger.error("Unable to write ConfigFile")
                break
            tries = tries+1
    return conf
