""" calculate an Absorptionsprofil of a picture
or calculate the reconstruction """
import math
from typing import Tuple, List, Any
from matplotlib import pylab as plt
import numpy as np
import radontea
from ct_simulation import configloader as conf
from ct_simulation import filehandler

def get_absorptionprofile(x1: int, y1: int, x2: int, y2: int, img_cropped_gray: np.ndarray, pixel_iteration: int = 10) -> np.ndarray:
    """ get an Absorptionsprofil of a picture
    Fills an array with information
    if there is the laser line or not """


    if 0 < pixel_iteration <= 25:
        pass
    else:
        raise ValueError("\
        pixel_iteration should not be greater than 50, or elsewise \
                         the absorption profile is not good enough.")

    img_line = np.array(img_cropped_gray)
    ap_arr = np.array([], dtype=int)
    aparr_iterator = ap_arr.size  # also 0
    mean_pixel = np.mean(img_line, axis=(0, 1))

    for j in range(x1, x2, pixel_iteration):
        redundance = 0
        for i in range(y1, y2):
            pixeldifference = img_line[i, j] - mean_pixel
            if pixeldifference >= 15.0:
                redundance += 1
                last_detected_pixel = i  # for logging the correct pixelvalue of the line

        if ap_arr.size == aparr_iterator:
            if redundance >= 1:
                pixel_brightness = round(img_line[last_detected_pixel - math.floor(redundance / 2), j], 4) * 0.01
                ap_arr = np.append(ap_arr, pixel_brightness)
            elif redundance == 0:
                pixel_brightness = round(img_line[y2-math.floor(y1/2), j], 4) * 0.01
                ap_arr = np.append(ap_arr, pixel_brightness)
        aparr_iterator += 1
    return ap_arr


# https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html
plt.rc('image', cmap='gray')  # to set the colormap default to gray


class CalcReconstruction:
    """ calculates the Reconstruction """

    def __init__(self):
        self.logger = conf.LogFile.get_logfile()

    def get_reconstruction(self, ap_arrays: np.ndarray) -> Tuple[str, str]:
        """ saves and returns the sinogram and the reconstruction """
        amount_angles = len(ap_arrays)
        angles = np.linspace(0, np.pi, amount_angles) # number of sinogram angles in radians (numerical)
        angles *= 2
        sino = ap_arrays*255
        reconstruction_fbp = radontea.backproject(sino, angles)
        self.logger.info(f"amount APS: {sino.shape[0]}, proved pixels per AP: {sino.shape[1]}")
        self.logger.info(f"Reconstruction Picure Sice: {reconstruction_fbp.shape[0]}*{reconstruction_fbp.shape[1]}")
        # for visualisation
        # plt.figure(figsize=(8, 12))
        # plt.subplot(6, 2, 3, title="sinogram (from original)")
        # plt.imshow(sino)
        # plt.subplot(6, 2, 5, title="filtered backprojection")
        # plt.imshow(reconstruction_fbp)
        # plt.tight_layout()
        # plt.show()

        imgpath_recon = filehandler.save_img_path(
            'reconstruction', 'jpg', newfolder=False, folder_name='/reconstruction-sino')
        plt.imsave(imgpath_recon, reconstruction_fbp, cmap="gray")
        imgpath_sino = filehandler.save_img_path('sinogram', 'jpg', newfolder=False, folder_name='/reconstruction-sino')
        plt.imsave(imgpath_sino, sino, cmap="gray")

        return imgpath_recon, imgpath_sino


def calculate_mean_calibration(list_calibrationaps: np.ndarray) -> list:
    """ adds all calibration absorption profiles together and return the mean of them"""
    mean_calibration = np.asarray(list_calibrationaps[0])
    for i in range(1, len(list_calibrationaps)):
        mean_calibration += list_calibrationaps[i]
    return mean_calibration/len(list_calibrationaps)


def correct_aps(aps: list) -> list:
    """ tries to enhance the visuality of absorption profiles """
    for k, cur_abs in enumerate(aps):  # for 100% aps
        # if max(cur_abs) - min(cur_abs) < 5:
        #     for i in range(len(cur_abs)):
        #         if 10 < i < len(cur_abs) - 10:
        #             temp_mean = 0
        #             for j in range(-10, 11):
        #                 if cur_abs[i + j] < 0.09:
        #                     temp_mean += 1
        #                 if temp_mean >= 11:
        #                     cur_abs[i] = cur_abs[i] + 100
        # else:  # for semitransparent aps
        #     for i in range(len(cur_abs)):
        #         if 10 < i < len(cur_abs) - 10:
        #             temp_mean = 0
        #             for j in range(-10, 11):
        #                 if cur_abs[i + j] > 5:
        #                     temp_mean += 1
        #                 if temp_mean >= 11:
        #                     cur_abs[i] = cur_abs[i] * 100
        cur_abs *= (-1)
        cur_abs += 1
    return aps
