#!/usr/bin/env python3
"""UI of the programm"""

import sys
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import time
from PyQt5.QtWidgets import QMainWindow, QApplication,\
    QPushButton, QGroupBox, QRadioButton,\
    QComboBox, QLabel, QSpinBox, QWidget, QGridLayout, \
    QProgressBar
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import QRect, QCoreApplication, QSize, \
    Qt, QThread, pyqtSignal
from typing import List
from ct_simulation import linedetection
from ct_simulation import hardware
from ct_simulation import configloader as conf
from ct_simulation import filehandler


def draw_ap(detectedlines: list) -> list:
    """
    zeichnet die Diagramm(Absorptionsprofile) und
    gibt die Pfade der Bilder zurueck
    """
    image_paths = []
    for line in range(len(detectedlines)):
        x = np.arange(0, len(detectedlines[line]))
        y = detectedlines[line]
        plt.ylim(0, 1)

        # Don't mess with the limits!

        plt.plot(x, y)

        path = filehandler.save_img_path(
            name='Bild'+str(line),
            newfolder=False,
            folder_name='/Bilder'
        )
        plt.savefig(path)
        image_paths.append(path)
        plt.clf()
    return image_paths


class App(QMainWindow):
    """The GUI-App"""

    def __init__(self) -> None:
        """Constructor for none GUI stuff"""
        super().__init__()
        self.image_array_index: int = 0
        # Content of the ImageFolder = Image Array
        self.content: List[str] = []
        self.calibrationcurve: List[str] = []
        self.winkel: List[str] = []
        self.sino_und_reco: bool = False
        self.winkel.append(0)
        self.left = 50
        self.top = 50
        self.width = 960
        self.height = 540

        self.logger = conf.LogFile.get_logfile()
        self.originpath = filehandler.get_img_series_folder(newimgseries=False, folder_name='/Bilder')
        self.originpath2 = filehandler.get_img_series_folder(newimgseries=False, folder_name='/reconstruction-sino')
        self.progress = 0
        self.init_ui()

    def init_ui(self) -> None:
        """the UI Constructor"""
        self.setWindowIcon(QIcon(str(Path(conf.__file__).parent)+'/config/Logo.png'))
        # self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.gridLayout = QGridLayout(self)
        self.gridLayout.setObjectName("gridLayout")
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)
        self.centralWidget().setLayout(self.gridLayout)

        # Radiobuttons for the Methods for the Hardware to choose
        self.radioButton_eA = QRadioButton(self)
        # self.radioButton_eA.setGeometry(QRect(30, 90, 200, 20))
        self.radioButton_eA.setObjectName("radioButton_eA")
        self.radioButton_eA.setChecked(True)

        self.radioButton_mA = QRadioButton(self)
        # self.radioButton_mA.setGeometry(QRect(30, 120, 230, 20))
        self.radioButton_mA.setObjectName("radioButton_mA")

        self.radioButton_rB = QRadioButton(self)
        # self.radioButton_rB.setGeometry(QRect(30, 150, 200, 20))
        self.radioButton_rB.setObjectName("radioButton_rB")

        # Parameters for the Hardware Methods

        # How many AP will be taken
        self.label_AnzahlAP = QLabel(self)
        # self.label_AnzahlAP.setGeometry(QRect(10, 30, 121, 31))
        self.label_AnzahlAP.setObjectName("label_AnzahlAP")
        self.spinBox_AnzahlAP = QSpinBox(self)
        self.spinBox_AnzahlAP.setEnabled(True)
        # self.spinBox_AnzahlAP.setGeometry(QRect(140, 40, 60, 22))
        self.spinBox_AnzahlAP.setMinimum(5)
        self.spinBox_AnzahlAP.setMaximum(300)
        self.spinBox_AnzahlAP.setSingleStep(1)
        self.spinBox_AnzahlAP.setObjectName("spinBox_AnzahlAP")

        # Kamera angular
        self.label_Kamerawinkel = QLabel(self)
        # self.label_Kamerawinkel.setGeometry(QRect(10, 80, 121, 31))
        self.label_Kamerawinkel.setObjectName("label_Kamerawinkel")
        self.spinBox_Kamerawinkel = QSpinBox(self)
        self.spinBox_Kamerawinkel.setEnabled(True)
        # self.spinBox_Kamerawinkel.setGeometry(QRect(140, 90, 60, 22))
        self.spinBox_Kamerawinkel.setMinimum(1)
        self.spinBox_Kamerawinkel.setMaximum(360)
        self.spinBox_Kamerawinkel.setSingleStep(1)
        self.spinBox_Kamerawinkel.setObjectName("spinBox_Kamerawinkel")

        # Description
        self.label_methode = QLabel(self)
        # self.label_methode.setGeometry(QRect(40, 10, 201, 16))
        self.label_methode.setObjectName("label_methode")

        # To Navigate the AP
        self.pushButton_APlinks = QPushButton(self)
        # self.pushButton_APlinks.setGeometry(QRect(400, 330, 40, 40))
        self.pushButton_APlinks.setObjectName("pushButton_APlinks")
        self.pushButton_APlinks.clicked.connect(self.ap_links)

        self.label_currentAP = QLabel(self)
        # self.label_currentAP.setGeometry(QRect(470, 330, 40, 40))
        self.label_currentAP.setObjectName("label_currentAP")
        self.label_currentAP.setAlignment(Qt.AlignCenter)

        self.label_AnzahlAP = QLabel(self)
        # self.label_AnzahlAP.setGeometry(QRect(10, 30, 121, 31))
        self.label_AnzahlAP.setObjectName("label_AnzahlAP")

        self.pushButton_APrechts = QPushButton(self)
        # self.pushButton_APrechts.setGeometry(QRect(520, 330, 40, 40))
        self.pushButton_APrechts.setObjectName("pushButton_APrechts")
        self.pushButton_APrechts.clicked.connect(self.ap_rechts)

        # To Navigate the RB
        self.pushButton_RBlinks = QPushButton(self)
        # self.pushButton_RBlinks.setGeometry(QRect(740, 330, 40, 40))
        self.pushButton_RBlinks.setObjectName("pushButton_RBlinks")
        self.pushButton_RBlinks.clicked.connect(self.rb_links)

        self.pushButton_RBrechts = QPushButton(self)
        # self.pushButton_RBrechts.setGeometry(QRect(820, 330, 40, 40))
        self.pushButton_RBrechts.setObjectName("pushButton_RBrechts")
        self.pushButton_RBrechts.clicked.connect(self.rb_rechts)

        # Button zur Hardware oder hardware simulation wenn ohne raspberry
        self.pushButton_best = QPushButton(self)
        # self.pushbutton_best.setGeometry(QRect(70, 390, 161, 61))
        self.pushButton_best.setObjectName("pushButton_best")

        self.pushButton_best.clicked.connect(self.make_image)
        # self.pushButton_best.clicked.connect(self.draw_and_show_ap)

        # Button für reset
        self.pushButton_startstopp = QPushButton(self)
        # self.pushbutton_startstopp.setGeometry(QRect(420, 430, 120, 80))
        self.pushButton_startstopp.setObjectName("pushbutton_startstopp")
        self.pushButton_startstopp.clicked.connect(self.reset)

        # Bildanzeige
        self.label_APBild = QLabel(self)
        # self.label_APBild.setGeometry(QRect(330, 20, 250, 250))
        self.label_APBild.setText("")
        self.label_APBild.setMaximumSize(QSize(600, 400))
        self.label_APBild.setPixmap(QPixmap(self.originpath + " /Bild"))
        self.label_APBild.setScaledContents(True)
        self.label_APBild.setObjectName("label_APBild")

        self.label_RBBild = QLabel(self)
        # self.label_RBBild.setGeometry(QRect(650, 20, 250, 250))
        self.label_APBild.setMaximumSize(QSize(600, 400))
        self.label_RBBild.setText("")
        self.label_RBBild.setPixmap(QPixmap(self.originpath + " /Bild"))
        self.label_RBBild.setScaledContents(True)
        self.label_RBBild.setObjectName("label_RBBild")

        self.label_winkelanzeige = QLabel(self)
        # self.label_winkelanzeige.setGeometry(QRect(650, 410, 220, 100))
        self.label_winkelanzeige.setObjectName("label_winkelanzeige")
        self.label_winkelanzeige.setText(str(self.winkel[0])+" Grad")

        self.label_winkelanzeigeb = QLabel(self)
        # self.label_winkelanzeigeb.setGeometry(QRect(725, 380, 180, 150))
        self.label_winkelanzeigeb.setText("")
        self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/0"))
        self.label_winkelanzeigeb.setMaximumSize(QSize(300, 200))
        self.label_winkelanzeigeb.setScaledContents(True)
        self.label_winkelanzeigeb.setObjectName("label_winkelanzeigeb")

        # Progressbar
        self.pbar = QProgressBar(self)

        self.Button_array = [self.pushButton_startstopp, self.pushButton_best, self.pushButton_APlinks,
                             self.pushButton_RBlinks, self.pushButton_APrechts, self.pushButton_RBrechts]

        # Grid Layout
        self.gridLayout.addWidget(self.label_Kamerawinkel, 9, 0, 1, 1)
        self.gridLayout.addWidget(self.label_RBBild, 0, 4, 9, 2)
        self.gridLayout.addWidget(self.radioButton_rB, 5, 0, 1, 1)
        self.gridLayout.addWidget(self.spinBox_Kamerawinkel, 10, 0, 1, 1)
        self.gridLayout.addWidget(self.label_APBild, 0, 1, 9, 3)
        self.gridLayout.addWidget(self.label_AnzahlAP, 13, 0, 1, 1)
        self.gridLayout.addWidget(self.pushButton_best, 16, 0, 1, 1)
        self.gridLayout.addWidget(self.radioButton_mA, 4, 0, 1, 1)
        self.gridLayout.addWidget(self.label_methode, 1, 0, 1, 1)
        self.gridLayout.addWidget(self.spinBox_AnzahlAP, 14, 0, 1, 1)
        self.gridLayout.addWidget(self.radioButton_eA, 3, 0, 1, 1)
        self.gridLayout.addWidget(self.label_currentAP, 9, 2, 1, 1)
        self.gridLayout.addWidget(self.pushButton_startstopp, 13, 2, 1, 1)
        self.gridLayout.addWidget(self.pushButton_APrechts, 9, 3, 1, 1)
        self.gridLayout.addWidget(self.pushButton_APlinks, 9, 1, 1, 1)
        self.gridLayout.addWidget(self.pushButton_RBrechts, 9, 5, 1, 1)
        self.gridLayout.addWidget(self.pushButton_RBlinks, 9, 4, 1, 1)
        self.gridLayout.addWidget(self.label_winkelanzeigeb, 12, 5, 5, 1)
        self.gridLayout.addWidget(self.label_winkelanzeige, 13, 4, 1, 1)
        self.gridLayout.addWidget(self.pbar, 15, 0, 1, 1)

        self.retranslate_ui()
        # Show Not needed because it will be called in def main()
        # self.show()

    def retranslate_ui(self) -> None:
        """TODO I don't know what this method does"""
        _translate = QCoreApplication.translate
        self.setWindowTitle(_translate("UI", "CT-Simulation"))
        self.radioButton_eA.setText(_translate("UI", "ein Absorptionsprofil"))
        self.radioButton_mA.setText(_translate("UI", "mehrere Absorptionsprofile"))
        self.radioButton_rB.setText(_translate("UI", "rekonstruiertes Bild"))
        self.pushButton_best.setText(_translate("UI", "Bestätigen"))

        # Sets the Label for the Camera Options
        self.label_AnzahlAP.setText(_translate("UI", "Anzahl der AP:"))
        self.label_Kamerawinkel.setText(_translate("UI", "Kamerawinkel:"))
        self.label_methode.setText(_translate("UI", "Wähle eine Methode"))
        self.pushButton_APlinks.setText(_translate("UI", "<--"))
        self.label_currentAP.setText(_translate("UI", str(self.image_array_index) + "/" + str(len(self.content))))
        self.pushButton_APrechts.setText(_translate("UI", "-->"))
        self.pushButton_RBlinks.setText(_translate("UI", "Reko"))
        self.pushButton_RBrechts.setText(_translate("UI", "Sino"))
        self.pushButton_startstopp.setText(_translate("UI", "Reset/Eichkurve"))

        self.pbar.setValue(self.progress)

    def toggle_buttons(self) -> None:
        """If buttons are enabled disable them and vice versa"""
        for button in self.Button_array:
            if button.isEnabled():
                button.setEnabled(False)
            else:
                button.setEnabled(True)
        self.logger.debug('Toggled Buttons')

    def make_image(self) -> None:
        """Interacts with the Hardware interface"""
        self.update_progressbar(0)
        self.winkel.clear()
        self.winkel.append(0)
        self.toggle_buttons()
        self.update_progressbar(50)
        if(self.radioButton_eA.isChecked()):
            self.logger.info("eA is checked")
            self.logger.info(f"Winkel: {str(self.spinBox_Kamerawinkel.value())}")
            # print("eA is checked")
            # print(f"Winkel:  {self.spinBox_Kamerawinkel.value()}")
            pictures = hardware.takepictures(
                angular=self.spinBox_Kamerawinkel.value(), amount=-1, recon=False)
            self.content = draw_ap(pictures)

        if self.radioButton_mA.isChecked():
            self.logger.info("mA is checked")
            self.logger.info(f"Winkel: {str(self.spinBox_AnzahlAP.value())}")
            # print("mA is checked")
            # print("Anzahl an AP: " + str(self.spinBox_AnzahlAP.value()))
            pictures = hardware.takepictures(-1, self.spinBox_AnzahlAP.value(), recon=False)
            self.content = draw_ap(pictures)

        if(self.radioButton_rB.isChecked()):
            self.logger.info("rB is checked")
            self.logger.info(f"Anzahl an AP:  {str(self.spinBox_AnzahlAP.value())}")
            # print("rB is checked")
            # print("Anzahl an AP: " + str(self.spinBox_AnzahlAP.value()))
            pictures = hardware.takepictures(-1, self.spinBox_AnzahlAP.value(), recon=True)
            self.content = draw_ap(pictures)
            self.sino_und_reco = True

        if (len(self.content) == 1):
            self.winkel.append(float(self.spinBox_Kamerawinkel.value()))
        else:
            tempwinkel = 360/len(self.content)
            for bild in range(len(self.content)):
                self.winkel.append(int(round(tempwinkel * bild)))

        self.content.insert(0, self.originpath + " /Bild")

        self.image_array_index = 0

        self.ap_rechts()
        self.rb_links()
        self.toggle_buttons()

        self.update_progressbar(100)

    def update_progressbar(self, value):
        """Updates the percentage of the progress"""
        if value < 0:
            value = 0
        if value > 100:
            value = 100
        self.progress = value
        self.pbar.setValue(self.progress)

    def draw_and_show_ap(self):
        """ruft line detection auf und fügt den Winkel der Bilder hinzu"""
        self.winkel.clear()
        self.winkel.append(0)
        self.content = draw_ap(linedetection.call_gui())
        if (len(self.content) == 1):
            self.winkel.append(float(self.spinBox_Kamerawinkel.value()))
        else:
            tempwinkel = 360/len(self.content)
            for bild in range(len(self.content)):
                self.winkel.append(int(round(tempwinkel * bild)))

        self.content.insert(0, self.originpath + " /Bild")
        # print(self.winkel)
        # print(self.content)
        self.sino_und_reco = True
        self.ap_rechts()
        self.rb_links()

    def ap_links(self):
        """Moves to the next AP left in line and updates the Winkel (image and numbers)"""

        if self.image_array_index > 0:
            self.image_array_index = self.image_array_index - 1
            # print("New AP Index: "+str(self.image_array_index))
            # print("New AP Winkel: "+str(self.winkel[self.image_array_index]))
            self.label_APBild.setPixmap(QPixmap(self.content[self.image_array_index]))
            self.label_winkelanzeige.setText(str(self.winkel[self.image_array_index])+" Grad")

            if self.winkel[self.image_array_index] < 30:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/0.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 60 and self.winkel[self.image_array_index] >= 30:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/30.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 90 and self.winkel[self.image_array_index] >= 60:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/60.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 120 and self.winkel[self.image_array_index] >= 90:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/90.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 150 and self.winkel[self.image_array_index] >= 120:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/120.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 180 and self.winkel[self.image_array_index] >= 150:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/150.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 210 and self.winkel[self.image_array_index] >= 180:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/180.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 240 and self.winkel[self.image_array_index] >= 210:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/210.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 270 and self.winkel[self.image_array_index] >= 240:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/240.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 300 and self.winkel[self.image_array_index] >= 270:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/270.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 330 and self.winkel[self.image_array_index] >= 300:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/300.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] >= 330:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/330.png"))
                print("Anzeige geändert")
        self.label_currentAP.setText((str(self.image_array_index) + "/" + str(len(self.content)-1)))

    def ap_rechts(self):
        """Moves to the next AP right in line"""

        if self.image_array_index+1 < len(self.content):
            self.image_array_index = self.image_array_index + 1
            print("New AP Index: "+str(self.image_array_index))
            print("New Winkel: "+str(self.winkel[self.image_array_index]))
            self.label_APBild.setPixmap(QPixmap(self.content[self.image_array_index]))
            self.label_winkelanzeige.setText(str(self.winkel[self.image_array_index])+" Grad")

            if self.winkel[self.image_array_index] < 30:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/0.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 60 and self.winkel[self.image_array_index] >= 30:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/30.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 90 and self.winkel[self.image_array_index] >= 60:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/60.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 120 and self.winkel[self.image_array_index] >= 90:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/90.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 150 and self.winkel[self.image_array_index] >= 120:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/120.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 180 and self.winkel[self.image_array_index] >= 150:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/150.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 210 and self.winkel[self.image_array_index] >= 180:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/180.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 240 and self.winkel[self.image_array_index] >= 210:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/210.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 270 and self.winkel[self.image_array_index] >= 240:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/240.png"))
                print("Anzeige geändert")

            if self.winkel[self.image_array_index] < 300 and self.winkel[self.image_array_index] >= 270:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/270.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] < 330 and self.winkel[self.image_array_index] >= 300:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/300.png"))
                print("Anzeige geändert")
            if self.winkel[self.image_array_index] >= 330:
                self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/330.png"))
                print("Anzeige geändert")
        self.label_currentAP.setText((str(str(self.image_array_index) + "/" + str(len(self.content)-1))))

    def reset(self) -> None:
        """Removes the images from the gui"""
        self.toggle_buttons()
        self.update_progressbar(50)
        self.toggle_buttons()
        hardware.takepictures(amount=10, calibration=True)
        self.update_progressbar(100)
        self.image_array_index = 0

        self.winkel.clear()
        self.content.clear()
        self.label_RBBild.setPixmap(QPixmap(self.originpath + " /Bild.png"))
        self.label_winkelanzeigeb.setPixmap(QPixmap(str(Path(conf.__file__).parent)+"/Winkel/0.png"))
        self.logger.info(self.image_array_index)
        self.logger.info(self.winkel)
        self.logger.info(self.content)
        print(self.image_array_index)
        print(self.winkel)
        print(self.content)
        self.sino_und_reco = False

    def rb_rechts(self) -> None:
        """Sets the image to the sinogramm"""

        if self.sino_und_reco:
            self.label_RBBild.setPixmap(QPixmap(self.originpath2 + "/sinogram.jpg"))
            print(self.originpath2)

    def rb_links(self) -> None:
        """Sets the image to the reconstruction"""
        if self.sino_und_reco:
            self.label_RBBild.setPixmap(QPixmap(self.originpath2 + "/reconstruction.jpg"))
            print(self.originpath2)


def main() -> None:
    app = QApplication(sys.argv)
    ex = App()
    ex.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
