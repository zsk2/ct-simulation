""" get absorption profiles and reconstruction of a picture """
import cv2
import math
import numpy as np
import sys
from pathlib import Path
from typing import List
from ct_simulation import reconstruction
from ct_simulation import configloader as conf


class LineDetection:
    """ get multiple Absorptionsprofil of a picture """

    def __init__(self):
        self.logger = conf.LogFile.get_logfile()

    def reconstruction(self, list_pictures: List[str], list_calibration: List[str],
                       do_reconstruction: bool = False) -> list:
        """ iterates through all given pictures paths """

        self.logger.info("-----------------------------------AP-----------------------------------")

        self.logger.info(list_pictures)
        self.logger.info(
            f"Width: {cv2.imread(list_pictures[0]).shape[1]}, height: {cv2.imread(list_pictures[0]).shape[0]}")

        calibration_aps = np.asarray([self.return_one_ap_calibration(list_calibration[0])])
        # skipcq: PYL-W0621
        for i in range(1, len(list_calibration)):
            calibration_aps = np.append(calibration_aps,
                                        [self.return_one_ap_calibration(list_calibration[i])], axis=0)
        calibration_mean = reconstruction.calculate_mean_calibration(calibration_aps)

        # skipcq: PYL-W0621
        aps = np.asarray([self.return_one_ap(list_pictures[0], calibration_mean)])
        for j in range(1, len(list_pictures)):
            aps = np.append(aps, [self.return_one_ap(list_pictures[j], calibration_mean)], axis=0)
        if do_reconstruction:
            get_reconstr = reconstruction.CalcReconstruction()
            get_reconstr.get_reconstruction(aps)

        aps_showable = reconstruction.correct_aps(aps)

        return aps_showable

    def return_one_ap(self, path: str, calibration_curve: list) -> list:
        """ get multiple Absorptionsprofil of a picture
        log information and view the pictures, when needed"""
        img_cropped, img_cropped_gray = preprocess_for_line(path)
        x1L, y1L, x2L, y2L = get_line_coo(img_cropped, False)  # detecting the line
        absorptionprofil = reconstruction.get_absorptionprofile(x1L, y1L, x2L, y2L,
                                                                img_cropped_gray)
        return absorptionprofil/calibration_curve

    def return_one_ap_calibration(self, path: str):
        """ get multiple Absorptionsprofil of a picture
        log information and view the pictures, when needed"""
        img_cropped, img_cropped_gray = preprocess_for_line(path)
        x1L, y1L, x2L, y2L = get_line_coo(img_cropped, False)  # detecting the line
        absorptionprofil = reconstruction.get_absorptionprofile(x1L, y1L, x2L, y2L,
                                                                img_cropped_gray)
        # smooth the data
        # skipcq: PYL-W0621
        for i in range(len(absorptionprofil)):
            if 2 < i < len(absorptionprofil)-2:
                temp_mean = 0
                for j in range(-2, 3):
                    temp_mean += absorptionprofil[i+j]
                absorptionprofil[i] = temp_mean/5
        return absorptionprofil


def preprocess_for_line(path: str):
    """ blur the image, and take the gray version of it"""
    img = cv2.imread(path)
    # Convert to gray
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blur the image a little bit,
    # so that not every little thing which is a line gets detected
    img_blur = cv2.GaussianBlur(img_gray, (5, 5), 10)
    # edge detector
    img_canny = cv2.Canny(img_blur, 5, 50)  # img, threshold for lines

    x1, y1, x2, y2 = 0, 0, img.shape[1], img.shape[0]

    # getting the cropped image of the original image
    img_cropped_gray = img_gray[y1:y2, x1:x2]
    img_cropped = img_canny[y1:y2, x1:x2]  # height first, width second
    np.set_printoptions(threshold=sys.maxsize)  # print the full numpy array

    return img_cropped, img_cropped_gray


def get_line_coo(img_find_edges: list, show_detected_lines: bool = False) -> tuple:
    """ lines are being detected
    img_find_edges: image to detect the lines
    show_detected_lines: True to show the image with marked detected lines
    https://docs.opencv.org/3.4/d9/db0/tutorial_hough_lines.html
    """
    # skipcq: PYL-W0621
    cfg = conf.load_config()
    rho = cfg['Linedetection']['rho']  # distance resolution in pixels of the Hough grid
    threshold = cfg['Linedetection']['threshold']  # minimum number of votes (intersections in Hough grid cell)
    min_line_length = cfg['Linedetection']['min_line_lenght']  # minimum number of pixels making up a line
    max_line_gap = cfg['Linedetection']['max_line_gap']  # maximum gap in pixels between connectable line segments
    theta = np.pi / 180  # angular resolution in radians of the Hough grid
    line_image = np.copy(img_find_edges) * 0  # creating a blank to draw lines on

    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(
        img_find_edges,
        rho,
        theta,
        threshold,
        np.array([]),
        min_line_length,
        max_line_gap)
    minx1, miny1, maxx2, maxy2 = [], [], [], []
    if lines is None:
        raise ValueError("Can't detect any lines")
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 5)
            minx1.append(x1)
            miny1.append(y1)
            maxx2.append(x2)
            maxy2.append(y2)
    # the outer proportions of all detected lines
    x1 = min(minx1)
    y1 = min(miny1)
    x2 = max(maxx2)
    y2 = max(maxy2)

    # get rid of the bad outer line portion
    # by ignoring the line by 15% on each side
    x1 = int(math.ceil(img_find_edges.shape[1]*0.15))
    x2 = int(math.ceil(img_find_edges.shape[1]*0.85))
    # Draw the lines on the  image
    lines_edges = cv2.addWeighted(img_find_edges, 0.8, line_image, 1, 0)
    if show_detected_lines:
        cv2.imshow("LineEdges", lines_edges)
    return x1, y1, x2, y2


def call_gui() -> list:
    """ TEMP: for simulating Hardware """
    calibrationcurves = []
    # skipcq: PYL-W0621
    for i in range(0, 4):
        calibrationcurves.append(str(Path(conf.__file__).parent) +
                                 "/Bilder/calibrationcurve-pictures/calibrationcurve_" + str(i) + ".jpg")
    paths = []
    # skipcq: PYL-W0621
    for i in range(2, 29):
        if i < 10:
            paths.append(str(Path(conf.__file__).parent)+"/laser/guteBilder5/full_dice_few_dark_0" + str(i) + ".jpg")
        else:
            paths.append(str(Path(conf.__file__).parent)+"/laser/guteBilder5/full_dice_few_dark_" + str(i) + ".jpg")
    line_detection = LineDetection()
    # skipcq: PYL-W0621
    aps = line_detection.reconstruction(paths, calibrationcurves, True)

    return aps

#
# if __name__ == "__main__":
#
#     conf.StandardCalibrationCurves.copy_pictures()
#     cfg = conf.load_config()
#     CALIBRATIONCURVES = filehandler.get_imgs(cfg['Filehandler']['cali'])
#
#     LINEDETECTION = LineDetection()
#
#     PATHS = []
    # skipcq: PYL-W0621
    # for i in range(2, 16):
        # PATHS.append("ct_simulation/laser/guteBilder4/" + str(i) + "df.jpg")
        #
        # if i < 10:
        #     PATHS.append("ct_simulation/laser/guteBilder5/full_dice_few_dark_0" + str(i) + ".jpg")
        # else:
        #     PATHS.append("ct_simulation/laser/guteBilder5/full_dice_few_dark_" + str(i) + ".jpg")
        # if (i < 10):
        #     PATHS.append("../Bilder_absorption/fulldice_bright_0" + str(i) + ".jpg")
        # else:
        #     PATHS.append("../Bilder_absorption/fulldice_bright_" + str(i) + ".jpg")

    # skipcq: PYL-W0621
    # aps = LINEDETECTION.reconstruction(PATHS, CALIBRATIONCURVES, True)
    # aps = line_detection.reconstruction(CALIBRATIONCURVES, CALIBRATIONCURVES, True)

    # show APs with matplotlib
    # x = np.arange(0, len(aps[0]))
    # if len(aps) < 20:
    #     fig, axs = plt.subplots(len(aps))
    # else: fig, axs = plt.subplots(20)
    # fig.suptitle('Vertically stacked subplots-AP')
    # for i, abs in enumerate(aps):
    #     if i < 20:
    #
    #         axs[i].plot(x, abs)
    # plt.show()
