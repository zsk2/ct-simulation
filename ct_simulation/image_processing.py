""" Rapid Image Taking
    https://picamera.readthedocs.io/en/release-1.13/recipes2.html#rapid-capture-and-processing
"""
import io
# import threading

from ct_simulation import filehandler
from ct_simulation import configloader


class SplitFrames():
    """Gives the PiCamera a custom write method"""

    def __init__(self, calibrationcurve: bool = False):
        """
        :param calibrationcurve: is the imageseries a calibration curve
        """
        self.frame_num: int = 0
        self.output = None
        self.logger = configloader.LogFile.get_logfile()
        self.cfg = configloader.load_config()
        if calibrationcurve is False:
            self.imageseriespath: str = filehandler.get_img_series_folder(newimgseries=True)
        else:
            self.imageseriespath: str = filehandler.get_img_series_folder(
                newimgseries=True, folder_name=self.cfg['Filehandler']['cali'])
        self.logger.debug(f'imageseriespath is {self.imageseriespath}')

    def write(self, buf):
        """ Compares if there is a new frame and saves the output if it is"""
        if buf.startswith(b'\xff\xd8'):
            # Start of new frame; close the old one (if any) and
            # open a new output
            if self.output:
                self.output.close()
            self.frame_num += 1
            self.output = io.open(self.imageseriespath +
                                  '/'+'%02d_image.jpg' % self.frame_num, 'wb')
        self.output.write(buf)
