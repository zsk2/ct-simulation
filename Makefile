pclean: clean
	sudo rm -rf ./build ./ct_simulation.egg-info ./dist /usr/local/bin/ct-simulation

clean:
	rm -rf ./.idea ./.pytest_cache ./.spyproject ./ct/__pycache__ ./test/__pycache__ debug.log ./ct/debug.log

init:
	sudo apt install python3-opencv python3-pytest python3-pyqt5 python3-qtpy libatlas-base-dev python3-matplotlib python3-natsort
	sudo pip3 install -r requirements.txt

test:
	pytest

comment-hardware:
	sed -i '7s/^/#/g' ct/hardware.py &&
	sed -i '77s/^/#/g' ct/hardware.py

remove-comment-hardware:
	sed -i '7s/#/\ /g' ct/hardware.py &&
	sed -i '77s/#/\ /g' ct/hardware.py

install: init
	sudo python3 setup.py install

.PHONY: init clean test install pclean
