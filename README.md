# CT-Simulation

CT-Simulation ist ein Projekt, welches die Funktionsweise eines Computer Tomographen für den Unterricht visualisieren soll.
Es rekonstruiert das Objekt mithilfe von mehreren Abbildern vom Linienlaser (Schatten des Objektes).

[![DeepSource](https://deepsource.io/gl/zsk2/ct-simulation.svg/?label=active+issues&show_trend=true)](https://deepsource.io/gl/zsk2/ct-simulation/?ref=repository-badge)
[![pipeline status](https://gitlab.com/zsk2/ct-simulation/badges/master/pipeline.svg)](https://gitlab.com/zsk2/ct-simulation/-/commits/master)
[![coverage report](https://gitlab.com/zsk2/ct-simulation/badges/master/coverage.svg)](https://gitlab.com/zsk2/ct-simulation/-/commits/master)

# Dependencies

- python3.7
- make
- pyqt5
- opencv
- picamera

## How to install PiCamera for Development

### Windows cmd only
```shell
set READTHEDOCS=True

pip install picamera
```

### Linux

```shell
export READTHEDOCS=True

pip install picamera
```

# How to Install it

```shell
sudo make init
sudo make install
```

# How to Run it

Type this in your terminal
```shell
ct-simulation
```

# How to view Camera + Laser

Type this in your terminal
```shell
python -m ct_simulation.calibrate_camera_laser
```

[Installation](https://gitlab.com/zsk2/ct-simulation/-/wikis/Installation)
